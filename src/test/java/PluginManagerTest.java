import model.Plugin;
import model.PluginManager;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

public class PluginManagerTest {
    @Test
    public void loadThreePluginsTest() throws IOException, ClassNotFoundException, IllegalAccessException, InstantiationException {
        String pluginRootDirectory = new File("").toURI() + "out/artifacts/";
        PluginManager pluginManager = new PluginManager(pluginRootDirectory);
        Plugin first = pluginManager.load("firstPlugin", "SomePlugin");
        Plugin second = pluginManager.load("secondPlugin", "SomePlugin");
        Plugin third = pluginManager.load("firstPlugin", "AnotherPlugin");
        first.loadPlugin();
        second.loadPlugin();
        third.loadPlugin();
    }
}
