package model;

import java.io.IOException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

public class PluginManager {
    private final String pluginRootDirectory;

    public PluginManager(String pluginRootDirectory) {
        this.pluginRootDirectory = pluginRootDirectory;
    }

    public Plugin load(String pluginName, String pluginClassName) throws ClassNotFoundException, IllegalAccessException, InstantiationException, IOException {
        URL url = new URL(pluginRootDirectory + pluginName + "/");
        JarFile jarFile = new JarFile(url.toString()
                .replace("file:", "")
                + pluginName + ".jar");
        Enumeration<JarEntry> e = jarFile.entries();

        URL[] urls = {new URL("jar:" + url + pluginName + ".jar" + "!/")};
        URLClassLoader pluginLoader = URLClassLoader.newInstance(urls);
        while (e.hasMoreElements()) {
            JarEntry je = e.nextElement();
            if (je.isDirectory() || !je.getName().endsWith(".class")) {
                continue;
            }
            String className = je.getName().substring(0, je.getName().length() - 6);
            className = className.replace('/', '.');
            Class c = pluginLoader.loadClass(className);
            if (Arrays.asList(c.getInterfaces()).contains(Plugin.class)
                    && c.getName().equals(pluginClassName)) {
                return (Plugin) c.newInstance();
            }
        }
        return null;
    }
}
