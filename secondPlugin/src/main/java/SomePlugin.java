import model.Plugin;

public class SomePlugin implements Plugin {
    @Override
    public void loadPlugin() {
        System.out.println("Loaded SomePlugin, second.");
    }
}
