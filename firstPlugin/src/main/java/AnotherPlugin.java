import model.Plugin;

public class AnotherPlugin implements Plugin {
    @Override
    public void loadPlugin() {
        System.out.println("Loaded AnotherPlugin, first.");
    }
}
